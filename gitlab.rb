
external_url 'http://gitlab.alibabaei.ir'

nginx['listen_port'] = 80
nginx['listen_https'] = false
letsencrypt['enable'] = false

gitlab_rails['gitlab_ssh_host'] = 'gitlab.alibabaei.ir'
gitlab_rails['time_zone'] = 'UTC'

### GitLab Shell settings for GitLab
gitlab_rails['gitlab_shell_ssh_port'] = 2222
gitlab_rails['gitlab_shell_git_timeout'] = 800

#################### posgres ####################
gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'unicode'
gitlab_rails['db_database'] = 'gitlabhq_production'
gitlab_rails['db_pool'] = 10
gitlab_rails['db_username'] = 'postgres'
gitlab_rails['db_password'] = File.read('/run/secrets/postgresql-password.key').strip
gitlab_rails['db_host'] = 'postgres'
gitlab_rails['db_port'] = 5432

#################### redis ####################
gitlab_rails['redis_host'] = 'redis'
gitlab_rails['redis_port'] = 6379
gitlab_rails['redis_password'] = File.read('/run/secrets/redis-password.key').strip


#################### registry #################

### Settings used by GitLab application
registry_external_url 'http://registry.alibabaei.ir'

registry_nginx['listen_port'] = 80
registry_nginx['listen_https'] = false

gitlab_rails['registry_enabled'] = true
# gitlab_rails['registry_host'] = "registry.alibabaei-chaghal.ir"
# gitlab_rails['registry_port'] = "80"
gitlab_rails['registry_path'] = "/var/opt/gitlab/gitlab-rails/shared/registry"

### Settings used by Registry application
registry['enable'] = true
registry['registry_http_addr'] = "0.0.0.0:5000"
registry['debug_addr'] = "0.0.0.0:5001"

registry['database'] = {
  'enabled' => false, # Must be false!
}

registry['storage'] = {
    'filesystem' => {
      'rootdirectory' => "/var/opt/gitlab/registry/storage"
    },
    'maintenance' => {
      'readonly' => {
        'enabled' => false
      }
    }
}

